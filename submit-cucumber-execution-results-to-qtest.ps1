Expand-Archive -LiteralPath C:\Tricentis\Automation\GitLab-Runner\cache\jgrayson\Pipeline_Cucumber_Java\default\cache.zip -DestinationPath C:\Tricentis\Automation\GitLab-Runner\cache\jgrayson\Pipeline_Cucumber_Java\default -Force

$resultsFile = "C:\Tricentis\Automation\GitLab-Runner\cache\jgrayson\Pipeline_Cucumber_Java\default\target\cucumber-report.json"

$url = 'https://pulse-7.qtestnet.com/webhook/0fd02d1e-47fc-4c3d-83f4-740ed24f2005'

$content = Get-Content $resultsFile -Raw
$bytes = [System.Text.Encoding]::ASCII.GetBytes($content)
$payload = [System.Convert]::ToBase64String($bytes)

$body = @{
 'projectId' = '87143'
 'testcycle' = '2645040'
 'result' = $payload
}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-RestMethod -Body $body -Method 'Post' -Uri $url
